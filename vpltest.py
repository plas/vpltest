# -*- coding: utf-8 -*-

from __future__ import print_function
from codecs import open

import sys
import os.path
import json
from urllib.request import urlopen
import importlib
import inspect
import threading
from builtins import issubclass
try:
    import thread
except ImportError:
    import _thread as thread
import traceback
import argparse
from compileall import compile_dir


FAIL="FAIL"
TEST_FAILED="Test failed"
PASS="OK"

_known_test_libraries = ["vpltest.py", "edutest.py"]

class VPLRunner:
    def __init__(self, args=None):
        arg_parser = self.create_argument_parser()
        self.options = arg_parser.parse_args(args)

        # TODO: check that user can't override a provided file 
        # if pyc-file exists then discard py-file
        # TODO: also check that user doesn't override an installed module (eg. edutest)

    def run(self):
        def test_order_key(f):
            if f.__doc__:
                return f.__doc__.lower()
            else:
                return f.__name__.lower()
            
        
        results = []
        for test in sorted(self.get_tests(), key=test_order_key):
            try:
                self.run_test(test)
                exception = None
            except Exception:
                exception = sys.exc_info()
            
            # printing right away, because timeout may forbid function to complete
            test_output = self.format_case(test, exception) 
            print(test_output)
            results.append((test, exception, test_output))
        
        if not self.options.no_grade:
            print(self.format_grade(results))
            
    def format_case(self, test, exception):
        s = "<|--\n"
        s += "-" + self.format_title(test, exception) + "\n" 
        
        if exception:
            s += self.format_exception(exception) + "\n\n"
        else:
            succ = self.format_success(test) 
            if succ:
                s += succ + "\n" 
                                  
        s += "--|>\n"
        return s
    
    def format_grade(self, results):
        total_cases = len(results)
        failed_cases = len([1 for (_, exception, _) in results if exception is not None])
        passed_cases = total_cases - failed_cases
    
        max_grade = self.get_max_grade()
        if max_grade is not None:
            pass_ratio = passed_cases / float(total_cases)
            grade = max_grade * pass_ratio
            
            return "Grade :=>> %.1f" % grade
        else:
            return ""
    
    def format_title(self, test, exception):
        if exception:
            result = FAIL
        else:
            result = PASS
            
        return self.get_test_name(test) + " ... " + result
    
    def format_exception(self, exception):
        (exc_type, exc_value, exc_traceback) = exception
        message = str(exc_value)
        
        if issubclass(exc_type, AssertionError) and message:
            # If the assertion was caused by test code, then show only error message
            return message
        
        # TODO: handle timeout separately
        else:
            tb = self.extract_relevant_stacktrace_items(exc_traceback)
            ex_only = "".join(traceback.format_exception_only(exc_type, exc_value))
            ex_with_tb = "".join(traceback.format_list(tb)) + ex_only
                       
            return ex_only + "\n\n" + self.preformat_text(ex_with_tb) 
    
    def format_success(self, test):
        # Don't say anything about passed cases by default
        return ""
    
    def get_test_name(self, test):
        if hasattr(test, "__doc__") and test.__doc__:
            return test.__doc__
        elif hasattr(test, "__name__") and test.__name__:
            return test.__name__
        else:
            return str(test)
    
    
    
    def create_argument_parser(self):
        parser = argparse.ArgumentParser()
        parser.add_argument("--no-grade", action="store_true", default=False, 
                            help="Disables computing the grade")
        parser.add_argument("test_files", nargs="*")
        
        return parser
        
    
    def get_test_files(self):
        if len(self.options.test_files) > 0:
            return self.options.test_files
        else:
            return [name for name in self._get_provided_files() 
                if "test" in name.lower()
                    and (name.endswith(".py") or name.endswith(".pyc") or name.endswith(".pyo")) 
                    and name not in _known_test_libraries]
    
    def get_tests(self, filenames=None):
        if not filenames:
            filenames = self.get_test_files()
            
        result = []
        for filename in filenames:
            module_name, extension = os.path.splitext(filename)
            assert extension in [".py", ".pyc", ".pyo"] 
            m = importlib.import_module(module_name)
            for name in m.__dict__:
                if "test" in name.lower() and not name.startswith("_"):
                    obj = m.__dict__[name]
                    if callable(obj):
                        sig = inspect.signature(obj)
                        if len(sig.parameters) == 0:
                            result.append(obj)
                    
        return sorted(result, key=lambda test: self.get_test_name(test))
    
    def get_submitted_files(self):
        result = []
        for i in range(100):
            key = "VPL_SUBFILE" + str(i)
            if key in os.environ:
                result.append(os.environ[key])
            else:
                break
            
        return result
    
    
    def run_test(self, test):
        # TODO: check if the test already includes a timeout
        return self._run_test_with_timeout(test)
            
    def _run_test_with_timeout(self, test):    
        # http://stackoverflow.com/a/31667005/261181
        # TODO: find timeout from test or compute from environment variable
        timeout = 1.0
        timer = threading.Timer(timeout, thread.interrupt_main)
        timer.start()
        try:
            return test()
        finally:
            timer.cancel()     
    
    def _get_provided_files(self):
        submitted_files = self.get_submitted_files()
        return [file for file in os.listdir(".") 
                if os.path.isfile(file)
                and file not in submitted_files]
    
    
    def extract_relevant_stacktrace_items(self, tb):
        # skip vpltest.py frames
        tb_frames = traceback.extract_tb(tb)
        #return [f for f in tb_frames if not (f[0].endswith("vpltest.py"))]
        return tb_frames

    def get_max_grade(self):
        try:
            return float(os.environ["VPL_GRADEMAX"])
        except:
            return None
    
    def get_vpl_language(self):
        return os.environ.get("VPL_LANG", None)
    
    def preformat_text(self, s):
        s = ">" + s.replace("\n", "\n>")
        if not s.endswith("\n"):
            s = s + "\n"
        return s


def emulate_vpl():
    # Used when run outside VPL
    submitted = [name for name in os.listdir() if name.endswith(".py") and not "test" in name.lower()]
    if "VPL_SUBFILES" not in os.environ:
        for i, name in enumerate(submitted):
            os.environ["VPL_SUBFILE" + str(i)] = name
        
        os.environ["VPL_SUBFILES"] = " ".join(submitted)
        
    for key, value in [
        ("MOODLE_USER_ID", "12345"),
        ("MOODLE_USER_NAME", "Oskar Ohakas"),
        ("VPL_LANG", "et_EE.UTF-8"),
        ]:
        if key not in os.environ:
            os.environ[key] = value
            
    
class ExperimentRunner(VPLRunner):
    def create_argument_parser(self):
        parser = VPLRunner.create_argument_parser(self)
        parser.add_argument("--task-id", action="store", default=None)
        return parser
    
    def run(self):
        if self.options.task_id:
            base_url = "http://localhost:5000"
            
            precheck_url = base_url + "/log_submission/" + self.options.task_id
             
            precheck_result = self._precheck(precheck_url)
            
            if "ERROR" in precheck_result:
                self._print_msg(precheck_result["ERROR"])
                return
            else:
                print("PREC result", precheck_result)
                # Make result available for the test
                for key in precheck_result:
                    os.environ[key] = str(precheck_result[key])
                
                if "COMMENT" in precheck_result and precheck_result["COMMENT"].strip():
                    self._print_msg(precheck_result["COMMENT"], "Info")
                

        
            results = VPLRunner.run(self)
            
            response_url = base_url + "/log_response/" + precheck_result["submission_id"] 
            self._postcheck(response_url, results)

    def _precheck(self, url):
        print("PRECHECKING")
        files = {}
        for filename in self.get_submitted_files():
            try:
                with open(filename, encoding="utf-8") as f:
                    files[filename] = f.read()
            except:
                files[filename] = "<failed reading file with encoding=utf-8>" 
        
        data = dict(os.environ)
        
        if "MOODLE_USER_NAME" in data:
            data["CLEAN_MOODLE_USER_NAME"] = self._clean_name(data["MOODLE_USER_NAME"])
        
        data["submitted_files"] = files
        binary_data = json.dumps(data).encode(encoding='utf-8') 
        
        with urlopen(url, binary_data) as f:
            data = f.read() 
            with open("precheck_result", "wb") as f2:
                f2.write(data)
            return json.loads(data.decode("utf-8"))
    
    def _postcheck(self, url, run_results):
        
        results_for_export = []
        for (test, exception, full_output) in run_results:
            test_title = self.format_title(test, exception)
            exception_text = self.format_exception(exception)
            
        binary_data = json.dumps(run_results).encode(encoding="utf-8")
        with urlopen(url, binary_data) as f:
            data = f.read().decode("utf-8")
            if data != "OK":
                raise RuntimeError("_postcheck failure " + data)
        
    
    def _clean_name(self, s):
        return ''.join([c if ord(c) < 128 else '_' for c in s]).replace(" ", "_").replace("-", "_")

    def _print_msg(self, msg, title=None):
        print("<|--")
        if title:
            print("-", title)
            
        print(msg)
        print("--|>")
        print()
    
    
    
def _compile_in_current_dir():
    compile_dir(".", maxlevels=0, quiet=True)
    # This created compiled files in __pycache__.
    # In order to use them without sources, they need to be moved to current dir
    # without python version descriptor
    cache_dir = "__pycache__"
    if os.path.exists(cache_dir):
        for name in os.listdir(cache_dir):
            full_name = os.path.join(cache_dir, name)  
            base, ext = os.path.splitext(name)
            module_name = base[0:base.index(".")]
            new_name = module_name + ext
            if os.path.exists(new_name):
                os.remove(new_name)
            os.rename(full_name, new_name)

def _get_current_script_module_name():
    assert sys.argv[0].endswith(".py")
    main_script_path = sys.argv[0]
    main_script_basename = os.path.basename(main_script_path)
    module_name, _ = os.path.splitext(main_script_basename)
    return module_name
    

def compile_execution_files():
    # Compile all python files so that VPL can delete them later
    # TODO: check that at this point only provided files are available
    _compile_in_current_dir()
    
    
    # Create run command with the same executable and same arguments as current process.
    # Main .py file may get deleted by VPL, but pyc/pyo file should be there.
    # For this reason, this should be invoked by -m option
    cmd_parts = [sys.executable, "-m", _get_current_script_module_name()] + sys.argv[1:]
    run_command = " ".join(cmd_parts) # TODO: escaping
    
    
    # Compile vpl_execution
    filename = "vpl_execution"
    f = open(filename, mode="w", encoding="UTF-8")
    f.write('#!/bin/bash\n')
    f.write('export PYTHONIOENCODING=utf-8\n')
    f.write('source vpl_environment.sh\n')
    f.write('export LANG=en_US.UTF-8\n') # TODO: use VPL_LANG ???
    f.write(run_command + '\n') 
    f.close()
    os.chmod(filename, 0o755)


def run_or_compile(runner_class=VPLRunner):
    if "VPL_LANG" in os.environ and not os.path.exists("vpl_execution"):
        # We're on Jail and should compile vpl_execution first
        compile_execution_files()
    else:
        if "VPL_LANG" not in os.environ: # we're outside of VPL
            emulate_vpl()
        
        runner = runner_class()
        runner.run()

if __name__ == "__main__":
    run_or_compile()