import threading
try:
    import thread
except ImportError:
    import _thread as thread

def run_with_timeout(fun, *args):    
    # http://stackoverflow.com/a/31667005/261181
    # TODO: find timeout from test or compute from environment variable
    timeout = 1.0
    timer = threading.Timer(timeout, thread.interrupt_main)
    timer.start()
    try:
        return fun(*args)
    finally:
        timer.cancel()

def f():
    while True:
        x = 2
    print("valmis")

try:
    run_with_timeout(f)
except KeyboardInterrupt:
    raise TimeoutError()

